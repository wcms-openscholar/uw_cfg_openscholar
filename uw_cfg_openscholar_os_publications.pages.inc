<?php

/**
 * @file
 */

/**
 *
 */
function uw_cfg_openscholar_os_publications_overview() {

  $args = func_get_args();

  if (count($args) == 0) {
    // Get default types the user has selected.
    $defaults = variable_get('os_publications_filter_publication_types', array());

    // Filter out bad data.
    unset($defaults[-1]);

    if (count(array_filter($defaults))) {
      $args = array('type', array_filter($defaults));
    }
  }

  module_load_include('inc', 'biblio', 'includes/biblio.pages');
  $output = call_user_func_array('biblio_page', $args);

  // Remove the filter status message
  // unset($output['biblio_page']['header']['filter_status']);
  // Remove the sorting option
  // unset($output['biblio_page']['header']['sort_links']);
  // Remove the alphabetic filtering.
  unset($output['biblio_page']['header']['alpha_line']);

  // Handle caching
  // cache_set($ckey, $output, 'cache_biblio_lists');
  // We try to match the filter to a taxonomy term path.
  $path = vsite_get_purl() . '/' . substr($_GET['q'], strlen('publications/'));
  $source = drupal_lookup_path('source', $path);
  if (strpos($source, 'taxonomy/term') === 0) {
    // It is a term, so use the name of the term as the title.
    $tid = str_replace('taxonomy/term/', '', $source);
    $term = taxonomy_term_load($tid);
    $filter_value = $term->name;
    drupal_set_title(ucfirst($filter_value));
  }
  else {
    // Change the page title to match the menu entry.
    ctools_include('menu', 'os');

    $item = menu_get_item();
    $menu_item = os_menu_load_link_path($_GET['q']);

    if (count($item['map']) > 1) {
      // This is a sub patch of the publications. Check if the router is a valid
      // one.
      if (!$menu_item) {
        if (preg_match('/publications\/(type|year|author)/', $_GET['q'])) {
          return $output;
        }

        // This could a non valid menu item or non-exiting publication.
        drupal_set_title('');
        drupal_not_found();
        drupal_exit();
      }
    }

    drupal_set_title($menu_item['title']);
  }

  return $output;
}
