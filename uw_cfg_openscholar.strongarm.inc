<?php
/**
 * @file
 * uw_cfg_openscholar.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_cfg_openscholar_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'imageapi_optimize_jpegoptim';
  $strongarm->value = array(
    'status' => 1,
    'path' => '/usr/bin/jpegoptim',
    'weight' => '0',
    'settings' => array(
      'progressive' => '',
    ),
  );
  $export['imageapi_optimize_jpegoptim'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'imageapi_optimize_optipng';
  $strongarm->value = array(
    'status' => 0,
    'path' => '',
    'settings' => array(
      'level' => '5',
      'interlace' => '',
    ),
    'weight' => '1',
  );
  $export['imageapi_optimize_optipng'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'imageapi_optimize_service';
  $strongarm->value = 'internal';
  $export['imageapi_optimize_service'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'imageapi_optimize_toolkit';
  $strongarm->value = 'imagemagick';
  $export['imageapi_optimize_toolkit'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'image_toolkit';
  $strongarm->value = 'imageapi_optimize';
  $export['image_toolkit'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mathjax_cdn_url';
  $strongarm->value = 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS-MML_HTMLorMML';
  $export['mathjax_cdn_url'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mathjax_config_string';
  $strongarm->value = 'MathJax.Hub.Config({
  extensions: [\'tex2jax.js\'],
  jax: [\'input/TeX\'],
  MMLorHTML: { prefer: { Firefox: "MML" }},
  tex2jax: {
    displayMath: [ [\'\\\\[\',\'\\\\]\'] ],
    inlineMath: [ [\'\\\\(\',\'\\\\)\'] ],
    processEscapes: true,
    processClass: \'page\',
    ignoreClass: \'html\'
  },
  showProcessingMessages: false,
  messageStyle: \'none\'
});
';
  $export['mathjax_config_string'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mathjax_config_type';
  $strongarm->value = '1';
  $export['mathjax_config_type'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mathjax_use_cdn';
  $strongarm->value = 1;
  $export['mathjax_use_cdn'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_default_active_menus';
  $strongarm->value = array(
    0 => 'cp',
    1 => 'devel',
    2 => 'main-menu',
    3 => 'management',
    4 => 'navigation',
    5 => 'primary-menu',
    7 => 'user-menu',
  );
  $export['menu_default_active_menus'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'os_cp_theme';
  $strongarm->value = 'seven';
  $export['os_cp_theme'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'os_menus';
  $strongarm->value = array(
    'primary-menu' => 'Primary Menu',
    'hidden_menu' => 'Hidden',
  );
  $export['os_menus'] = $strongarm;

  return $export;
}
