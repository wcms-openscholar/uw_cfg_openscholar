<?php

/**
 * @file
 * uw_cfg_openscholar.form.inc
 */

/**
 * Form builder; checks if user has a uw_virutal_site_homepage and if so, redirect to the edit page,
 * if not then redirect to create uw_virtual_site_homepage.
 */
function uw_cfg_openscholar_homepage_settings_form($form_state) {
  global $base_path;

  // Get current link.
  $url = request_uri();

  // Ensure that we don't replace all the slashes if just at top root.
  if ($base_path !== '/') {
    // Ensure that we are at the very base.
    $url = str_replace($base_path, '', $url);
  }

  // Break Url into its parts.
  $url_parts = explode('/', $url);

  // The username should be the first element in the url_parts array, set it to username.
  // If the first element is blank use the second element.
  if ($url_parts[0] == '') {
    $username = $url_parts[1];
  }
  else {
    $username = $url_parts[0];
  }

  // Load in user by name.
  $user = user_load_by_name($username);

  // Get the uw_virtual_site_homepage nodes that belong to this user.
  // Is this first line needed? I noticed its missing from your example.
  $query = new EntityFieldQuery();
  $uw_virtual_site_homepage = $query
    ->entityCondition('entity_type', 'node')
    ->propertyCondition('status', 1)
    ->propertyCondition('type', 'uw_virtual_site_homepage')
    ->propertyCondition('uid', $user->uid)
    ->execute();

  // If there is a node, redirect to the edit page for that node.
  // If there is no node, then create a uw_virtual_site_homepage for that user.
  // This will ensure that there is only one uw_virtual_site_homepage per user.
  if (isset($uw_virtual_site_homepage) && count($uw_virtual_site_homepage) > 0) {
    foreach ($uw_virtual_site_homepage['node'] as $key => $uwvsh) {
      drupal_goto('node/' . $key . '/edit');
    }
  }
  else {
    drupal_goto('node/add/uw-virtual-site-homepage');
  }
}
