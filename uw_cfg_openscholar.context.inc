<?php
/**
 * @file
 * uw_cfg_openscholar.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_cfg_openscholar_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_os_front_page';
  $context->description = 'UW front page Open Scholar';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-uw_os_number_of_sites-block' => array(
          'module' => 'views',
          'delta' => 'uw_os_number_of_sites-block',
          'region' => 'content_top',
          'weight' => '-10',
        ),
        'uw_auth_cas_common-cas' => array(
          'module' => 'uw_auth_cas_common',
          'delta' => 'cas',
          'region' => 'footer_bottom',
          'weight' => '-10',
        ),
      ),
    ),
    'debug' => array(
      'debug' => 1,
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('UW front page Open Scholar');
  $export['uw_os_front_page'] = $context;

  return $export;
}
