<?php
/**
 * @file
 * uw_cfg_openscholar.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_cfg_openscholar_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_cfg_openscholar_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_filter_default_formats_alter().
 */
function uw_cfg_openscholar_filter_default_formats_alter(&$data) {
  if (isset($data['filtered_html'])) {
    $data['filtered_html']['filters']['ckeditor_link_filter'] = array(
      'settings' => array(),
      'status' => 1,
      'weight' => -49,
    ); /* WAS: '' */
    $data['filtered_html']['filters']['ckeditor_os_social_media'] = array(
      'settings' => array(),
      'status' => 1,
      'weight' => 0,
    ); /* WAS: '' */
    $data['filtered_html']['filters']['filter_htmlcorrector']['weight'] = -45; /* WAS: -46 */
    $data['filtered_html']['filters']['filter_mathjax'] = array(
      'settings' => array(),
      'status' => 1,
      'weight' => -36,
    ); /* WAS: '' */
    $data['filtered_html']['filters']['filter_url']['weight'] = -47; /* WAS: -48 */
    $data['filtered_html']['filters']['media_filter']['weight'] = -46; /* WAS: -47 */
    $data['filtered_html']['filters']['wysiwyg']['settings']['valid_elements'] = '@[id|style|class],
                                a[href|target<_blank|title|name],
                                div[align<center?justify?left?right],
                                p[dir|align<center?justify?left?right],
                                br,span,cite,code,blockquote,ul,ol,li,dl,dt,dd,img[!src|width|height|alt|title],
                                sup,sub,
                                table[scope|id|headers|style|border|align<center?left?right|cellpadding|cellspacing|bgcolor],
                                tr[scope|id|headers|align<center?justify?left?right|valign],
                                td[scope|id|headers|colspan|rowspan|align<center?justify?left?right|valign],
                                th[scope|id|headers|colspan|rowspan|align<center?justify?left?right|valign],
                                thead[scope|id|headers],
                                caption,figcaption,
                                address,h2,h3,h4,h5,h6,pre,
                                strong/b,em/i,strike,s,u,abbr[title]'; /* WAS: '@[id|style|class],
        a[href|target<_blank|title|name|rel|download],
        div[align<center?justify?left?right],
        p[dir|align<center?justify?left?right],
        br,span,cite,code,blockquote,ul,ol,li,dl,dt,dd,img[!src|width|height|alt|title],
        sup,sub,
        table[scope|id|headers|style|border|align<center?left?right|cellpadding|cellspacing|bgcolor],
        tr[scope|id|headers|align<center?justify?left?right|valign],
        td[scope|id|headers|colspan|rowspan|align<center?justify?left?right|valign],
        th[scope|id|headers|colspan|rowspan|align<center?justify?left?right|valign],
        thead[scope|id|headers],
        caption,figcaption,
        address,h2,h3,h4,h5,h6,pre,
        strong/b,em/i,strike,s,u,abbr[title]' */
    $data['filtered_html']['filters']['wysiwyg']['weight'] = -48; /* WAS: -49 */
    unset($data['filtered_html']['filters']['wysiwyg']['settings']['rule_valid_classes'][0]);
  }
}

/**
 * Implements hook_spaces_presets_alter().
 */
function uw_cfg_openscholar_spaces_presets_alter(&$data) {
  if (isset($data['os_scholar'])) {
    $data['os_scholar']->value['variable']['pathauto_node_biblio_pattern'] = '[vsite:site-purl]/publications/[node:title]'; /* WAS: '' */
    $data['os_scholar']->value['variable']['pathauto_node_blog_pattern'] = '[vsite:site-purl]/blog/[node:original:created:custom:Y]/[node:original:created:custom:m]/[node:title]'; /* WAS: '' */
    $data['os_scholar']->value['variable']['pathauto_node_class_material_pattern'] = '[vsite:site-purl]/classes/[node:field_class]/materials/[node:title]'; /* WAS: '' */
    $data['os_scholar']->value['variable']['pathauto_node_class_pattern'] = '[vsite:site-purl]/classes/[node:title]'; /* WAS: '' */
    $data['os_scholar']->value['variable']['pathauto_node_event_pattern'] = '[vsite:site-purl]/event/[node:title]'; /* WAS: '' */
    $data['os_scholar']->value['variable']['pathauto_node_faq_pattern'] = '[vsite:site-purl]/faq/[node:title]'; /* WAS: '' */
    $data['os_scholar']->value['variable']['pathauto_node_feed_importer_pattern'] = '[vsite:site-purl]/'; /* WAS: '' */
    $data['os_scholar']->value['variable']['pathauto_node_feed_pattern'] = '[vsite:site-purl]/feeds/[node:title]'; /* WAS: '' */
    $data['os_scholar']->value['variable']['pathauto_node_link_pattern'] = '[vsite:site-purl]/links/[node:title]'; /* WAS: '' */
    $data['os_scholar']->value['variable']['pathauto_node_news_pattern'] = '[vsite:site-purl]/news/[node:title]'; /* WAS: '' */
    $data['os_scholar']->value['variable']['pathauto_node_page_pattern'] = '[vsite:site-purl]/[node:title]'; /* WAS: '' */
    $data['os_scholar']->value['variable']['pathauto_node_person_pattern'] = '[vsite:site-purl]/people/[node:title]'; /* WAS: '' */
    $data['os_scholar']->value['variable']['pathauto_node_presentation_pattern'] = '[vsite:site-purl]/presentations/[node:title]'; /* WAS: '' */
    $data['os_scholar']->value['variable']['pathauto_node_software_project_pattern'] = '[vsite:site-purl]/software/[node:title]'; /* WAS: '' */
    $data['os_scholar']->value['variable']['pathauto_node_software_release_pattern'] = '[vsite:site-purl]/software/[node:field_software_project]/[node:field_software_version]'; /* WAS: '' */
    $data['os_scholar']->value['variable']['pathauto_taxonomy_term_pattern'] = '[vsite:site-purl]/[term:vocabulary]/[term:name]'; /* WAS: '' */
    $data['os_scholar']->value['variable']['pathauto_taxonomy_vocabulary_pattern'] = '[vsite:site-purl]/vocab/[vocabulary:name]'; /* WAS: '' */
    $data['os_scholar']->value['variable']['theme_default'] = 'uw_personal_site_theme'; /* WAS: 'hwpi_classic' */
  }
}

/**
 * Implements hook_strongarm_alter().
 */
function uw_cfg_openscholar_strongarm_alter(&$data) {
  if (isset($data['pathauto_update_action'])) {
    $data['pathauto_update_action']->value = 2; /* WAS: 0 */
  }
}

/**
 * Implements hook_wysiwyg_default_profiles_alter().
 */
function uw_cfg_openscholar_wysiwyg_default_profiles_alter(&$data) {
  if (isset($data['filtered_html'])) {
    $data['filtered_html']['name'] = 'formatfiltered_html'; /* WAS: '' */
    $data['filtered_html']['preferences'] = array(
      'add_to_summaries' => TRUE,
      'default' => 1,
      'show_toggle' => 1,
      'user_choose' => 0,
      'version' => '4.6.2.20af917',
    ); /* WAS: '' */
    $data['filtered_html']['settings']['buttons']['default']['Link'] = 1; /* WAS: '' */
    $data['filtered_html']['settings']['buttons']['drupal_path'] = array(
      'Link' => 1,
    ); /* WAS: '' */
    unset($data['filtered_html']['settings']['add_to_summaries']);
    unset($data['filtered_html']['settings']['buttons']['drupal']['os_link']);
    unset($data['filtered_html']['settings']['buttons']['drupal']['pastefromword']);
    unset($data['filtered_html']['settings']['default']);
    unset($data['filtered_html']['settings']['show_toggle']);
    unset($data['filtered_html']['settings']['user_choose']);
  }
}
