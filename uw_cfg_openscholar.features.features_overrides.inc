<?php
/**
 * @file
 * uw_cfg_openscholar.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_cfg_openscholar_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: filter
  $overrides["filter.filtered_html.filters|ckeditor_link_filter"] = array(
    'settings' => array(),
    'status' => 1,
    'weight' => -49,
  );
  $overrides["filter.filtered_html.filters|ckeditor_os_social_media"] = array(
    'settings' => array(),
    'status' => 1,
    'weight' => 0,
  );
  $overrides["filter.filtered_html.filters|filter_htmlcorrector|weight"] = -45;
  $overrides["filter.filtered_html.filters|filter_mathjax"] = array(
    'settings' => array(),
    'status' => 1,
    'weight' => -36,
  );
  $overrides["filter.filtered_html.filters|filter_url|weight"] = -47;
  $overrides["filter.filtered_html.filters|media_filter|weight"] = -46;
  $overrides["filter.filtered_html.filters|wysiwyg|settings|rule_valid_classes|0"]["DELETED"] = TRUE;
  $overrides["filter.filtered_html.filters|wysiwyg|settings|valid_elements"] = '@[id|style|class],
                              a[href|target<_blank|title|name],
                              div[align<center?justify?left?right],
                              p[dir|align<center?justify?left?right],
                              br,span,cite,code,blockquote,ul,ol,li,dl,dt,dd,img[!src|width|height|alt|title],
                              sup,sub,
                              table[scope|id|headers|style|border|align<center?left?right|cellpadding|cellspacing|bgcolor],
                              tr[scope|id|headers|align<center?justify?left?right|valign],
                              td[scope|id|headers|colspan|rowspan|align<center?justify?left?right|valign],
                              th[scope|id|headers|colspan|rowspan|align<center?justify?left?right|valign],
                              thead[scope|id|headers],
                              caption,figcaption,
                              address,h2,h3,h4,h5,h6,pre,
                              strong/b,em/i,strike,s,u,abbr[title]';
  $overrides["filter.filtered_html.filters|wysiwyg|weight"] = -48;

  // Exported overrides for: spaces_presets
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_node_biblio_pattern"] = '[vsite:site-purl]/publications/[node:title]';
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_node_blog_pattern"] = '[vsite:site-purl]/blog/[node:original:created:custom:Y]/[node:original:created:custom:m]/[node:title]';
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_node_class_material_pattern"] = '[vsite:site-purl]/classes/[node:field_class]/materials/[node:title]';
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_node_class_pattern"] = '[vsite:site-purl]/classes/[node:title]';
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_node_event_pattern"] = '[vsite:site-purl]/event/[node:title]';
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_node_faq_pattern"] = '[vsite:site-purl]/faq/[node:title]';
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_node_feed_importer_pattern"] = '[vsite:site-purl]/';
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_node_feed_pattern"] = '[vsite:site-purl]/feeds/[node:title]';
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_node_link_pattern"] = '[vsite:site-purl]/links/[node:title]';
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_node_news_pattern"] = '[vsite:site-purl]/news/[node:title]';
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_node_page_pattern"] = '[vsite:site-purl]/[node:title]';
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_node_person_pattern"] = '[vsite:site-purl]/people/[node:title]';
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_node_presentation_pattern"] = '[vsite:site-purl]/presentations/[node:title]';
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_node_software_project_pattern"] = '[vsite:site-purl]/software/[node:title]';
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_node_software_release_pattern"] = '[vsite:site-purl]/software/[node:field_software_project]/[node:field_software_version]';
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_taxonomy_term_pattern"] = '[vsite:site-purl]/[term:vocabulary]/[term:name]';
  $overrides["spaces_presets.os_scholar.value|variable|pathauto_taxonomy_vocabulary_pattern"] = '[vsite:site-purl]/vocab/[vocabulary:name]';
  $overrides["spaces_presets.os_scholar.value|variable|theme_default"] = 'uw_personal_site_theme';

  // Exported overrides for: variable
  $overrides["variable.pathauto_update_action.value"] = 2;

  // Exported overrides for: wysiwyg
  $overrides["wysiwyg.filtered_html.name"] = 'formatfiltered_html';
  $overrides["wysiwyg.filtered_html.preferences"] = array(
    'add_to_summaries' => TRUE,
    'default' => 1,
    'show_toggle' => 1,
    'user_choose' => 0,
    'version' => '4.6.2.20af917',
  );
  $overrides["wysiwyg.filtered_html.settings|add_to_summaries"]["DELETED"] = TRUE;
  $overrides["wysiwyg.filtered_html.settings|buttons|default|Link"] = 1;
  $overrides["wysiwyg.filtered_html.settings|buttons|drupal_path"] = array(
    'Link' => 1,
  );
  $overrides["wysiwyg.filtered_html.settings|buttons|drupal|os_link"]["DELETED"] = TRUE;
  $overrides["wysiwyg.filtered_html.settings|buttons|drupal|pastefromword"]["DELETED"] = TRUE;
  $overrides["wysiwyg.filtered_html.settings|default"]["DELETED"] = TRUE;
  $overrides["wysiwyg.filtered_html.settings|show_toggle"]["DELETED"] = TRUE;
  $overrides["wysiwyg.filtered_html.settings|user_choose"]["DELETED"] = TRUE;

 return $overrides;
}
