<?php

/**
 * @file
 * Create documentation links at the top of all standard content types.
 * Called from within hook_form_alter(&$form, &$form_state, $form_id).
 */

// Define the machine name of every content creation page, the "friendly" name, and the URL to the documentation.
$documentation = [
  'page_node_form' => [
    'name' => 'page',
    'url' => 'https://uwaterloo.ca/web-resources/scholar/add-page',
  ],
  'blog_node_form' => [
    'name' => 'blog',
    'url' => 'https://uwaterloo.ca/web-resources/scholar/create-blog-entry',
  ],
  'class_node_form' => [
    'name' => 'class',
    'url' => 'https://uwaterloo.ca/web-resources/scholar/add-class',
  ],
  'event_node_form' => [
    'name' => 'event',
    'url' => 'https://uwaterloo.ca/web-resources/scholar/create-event',
  ],
  'faq_node_form' => [
    'name' => 'faq',
    'url' => 'https://uwaterloo.ca/web-resources/scholar/create-faq',
  ],
  'link_node_form' => [
    'name' => 'link',
    'url' => 'https://uwaterloo.ca/web-resources/scholar/create-link',
  ],
  'news_node_form' => [
    'name' => 'news',
    'url' => 'https://uwaterloo.ca/web-resources/scholar/create-news-item',
  ],
  'person_node_form' => [
    'name' => 'person',
    'url' => 'https://uwaterloo.ca/web-resources/scholar/create-person-profile',
  ],
  'presentation_node_form' => [
    'name' => 'presentation',
    'url' => 'https://uwaterloo.ca/web-resources/scholar/create-person-profile',
  ],
  'biblio_node_form' => [
    'name' => 'publication',
    'url' => 'https://uwaterloo.ca/web-resources/scholar/create-publication',
  ],
  'software_project_node_form' => [
    'name' => 'software project',
    'url' => 'https://uwaterloo.ca/web-resources/scholar/create-software-project',
  ],
];

// Add documentation information if we're on a form that we have documentation for.
if (in_array($form_id, array_keys($documentation))) {
  // Create a prefix if one doesn't already exist. We add to it later. We want to make sure we don't accidentally remove a pre-existing one.
  if (!isset($form['#prefix'])) {
    $form['#prefix'] = '';
  }
  // Generate the link.
  $link = l($documentation[$form_id]['name'] . ' documentation', $documentation[$form_id]['url'], array('attributes' => array('target' => '_blank')));
  $form['#prefix'] .= '<p class="uw-documentation">Need help? Check out our ' . $link . '. (Link will open in a new window or tab.)</p>';
}
