(function ($) {

  Drupal.behaviors.image_paste = {

    attach: function (context, settings) {

      CKEDITOR.on('instanceReady', function (ev) {

        // Remove all images from pasted content.
        ev.editor.on('paste', function (ev) {

          // Do the right operation for the right data.
          if (ev.data.dataValue) {
            ev.data.dataValue = ev.data.dataValue.replace(/<img( [^>]*)?>/gi, '');
          }
          if (ev.data.dataValue) {
            ev.data.dataValue = ev.data.dataValue.replace(/<img( [^>]*)?>/gi, '');
          }
        });

        // Prevent drag-and-drop from outside CKEditor, but allow it from inside.
        // By default, assume drags start outside the editor.
        // Make dragstart_outside a global variable to allow dragging between instances of the editor.
        var dragstart_outside = true;
        // When a drag starts inside the editor, thus firing dragstart, record this in dragstart_outside.
        ev.editor.document.on('dragstart', function (ev) {
          dragstart_outside = false;
        });
        // Prevent drops that start outside, reset dragstart_outside.
        ev.editor.document.on('drop', function (ev) {
          if (dragstart_outside) {
            ev.data.preventDefault(true);
          }
          dragstart_outside = true;
        });

        // Remove empty p and h# tags.
        ev.editor.on('getData', function (ev) {
          ev.data.dataValue = ev.data.dataValue.replace(/<(p|h[1-6])>&nbsp;<\/(p|h[1-6])>/gi, '');
        });

        // When we change modes (to/from source view), call getData to strip out empty p and h# tags
        // this breaks some things - maybe a different event? Event Summary @ http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.editor.html
        ev.editor.on('mode', function (ev) {
          // ev.editor.setData(ev.editor.getData());
        });

        ev.editor.document.on('DOMNodeInserted', function (ev) {
          var target = ev.data.getTarget();
          if (target.is && target.is('img') && target.getAttribute('src') && target.getAttribute('src').substr(0, 5) === 'data:' && !target.getAttribute('data-cke-real-element-type')) {
            target.remove();
          }
        });

      });

    }
  };
})(jQuery);
