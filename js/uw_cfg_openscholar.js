/**
 * @file
 */

(function ($) {

  Drupal.behaviors.uw_cfg_openscholar = {

    attach: function (context, settings) {

      // Modifying publications to change Add person to Add contributor.
      if ($('#edit-biblio-authors #edit-add-more').length) {
        $('#edit-biblio-authors #edit-add-more').attr('value', 'Add contributor');
      }

      // Modify biblio year when clicked through for Submitted and Forthcoming.
      // Also if the radio is switched to published ensure that the year is blanked out.
      $('#edit-biblio-year-coded').change(function () {

        // In Press/Accepted.
        if ($("input[name='biblio_year_coded']:checked").attr('id') == "edit-biblio-year-coded-10000") {
          $("input[name='biblio_year']").val(9998);
        }

        // Submitted.
        if ($("input[name='biblio_year_coded']:checked").attr('id') == "edit-biblio-year-coded-10010") {
          $("input[name='biblio_year']").val(9999);
        }

        // Published.
        if ($("input[name='biblio_year_coded']:checked").attr('id') == "edit-biblio-year-coded-0") {
          $("input[name='biblio_year']").val('');
        }

      });

      // Modify biblio year when page loads for Submitted and Forthcoming.
      $(document).ready(function () {
        if ($("input[name='biblio_year']").length) {

          // In Press/Accepted.
          if ($("input[name='biblio_year']").val() == 10000) {
            $("input[name='biblio_year']").val(9998);
          }

          // Submitted.
          if ($("input[name='biblio_year']").val() == 10010) {
            $("input[name='biblio_year']").val(9999);
          }

        }
      });

      // On node add forms, use AJAX callback to generate alias preview.
      $('.page-node-add #edit-title, .page-node-edit #edit-title').change(function () {
        alias_preview_description_init();
        alias_preview_ajax_handler();
      });

      /**
       * Adds the AJAX handler to generate pathauto aliases for preview.
       */
      function alias_preview_ajax_handler() {
        // Verifies settings before continuing...
        if (Drupal.settings.alias_preview && Drupal.settings.alias_preview.make_alias) {
          // Prepares the ajax callback URL to query.
          var href = Drupal.settings.alias_preview.url;
          // Prepares the URL parameters to call for this node.
          var data = Drupal.settings.alias_preview;
          // Cleans the title value stored in our URL params.
          data.title = $.trim($('#edit-title').attr('value'));

          // Only proceeds if there is a title value, and
          // if the "Generate automatic alias" setting is checked.
          if (data.title.length > 0 && $('#edit-path-pathauto').attr('checked')) {
            // Adds autocomplete-style default Drupal throbber.
            $('.form-item-title .description').html('<strong>Link URL:</strong> <span class="ajax-progress"><span class="throbber"></span></span>');
            // Fetches the generated alias from the menu callback.
            $.getJSON(href, data, function (json) {
              // If we got a successful AJAX response...
              if (json.status) {

                // Prepares the alias, removing purl.
                var alias = json.data;
                var base_url = Drupal.settings.alias_preview.prefix;
                var purl = Drupal.settings.alias_preview.purl;
                if (purl.length) {
                  alias = alias.slice(purl.length + 1);
                }

                // Updates the existing pathauto alias field with the new value.
                $('#edit-path-alias').attr('value', alias);
                // Updates the title input field description to immediately show user.
                var description = '<strong>Link URL:</strong> ' + base_url + '/' + alias + ' <a id="pathauto-extra-edit-path" href="#path[pathauto]">edit</a>';
                $('.form-item-title .description').html(description);

                alias_preview_scroll();
              }
            });
          }
        }
      }

      /**
       * Ensures that the description <div> exists below the title.
       *
       * It won't exist at first when the page loads.
       */
      function alias_preview_description_init() {
        if (! $('.form-item-title .description').length) {
          $('<div class="description"></div>').insertAfter('#edit-title');
        }
      }

    }
  };
})(jQuery);
